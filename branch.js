const branch = (predicate, val1, val2) => x =>
  predicate(x)
    ? typeof val1 === 'function'
      ? val1(x)
      : val1
    : typeof val2 === 'function'
    ? val2(x)
    : val2;
