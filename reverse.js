const reverse =
  fn =>
  (...args) =>
    fn.apply(this, args.reverse());

module.exports = reverse;
