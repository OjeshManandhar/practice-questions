const compose = require('./compose');
const reverse = require('./reverse');

const leftCompose = (...args) => reverse(compose)(...args);
