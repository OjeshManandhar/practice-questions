const filter = (filterFunc, arr) => {
  const newArr = [];

  for (let i = 0; i < arr.length; i++) {
    if (filterFunc(arr[i], i, arr, this)) newArr.push(arr[i]);
  }

  return newArr;
};
