const compose = require('./compose');

const curry = func => {
  const curried = (...args) => {
    if (args.length >= func.length) return func.apply(this, args);

    return (...args2) => curried.apply(this, args.concat(args2));
  };

  return curried;
};

const curryVarLength = func => {
  const argsList = [];

  const curried = (...args) => {
    if (args.length === 0) return func.apply(this, argsList);

    argsList.push(...args);

    return (...args2) => curried.apply(this, args2);
  };

  return curried;
};

// const sum = (a, b, c) => a + b + c;
// const curried = curry(sum);

// console.log('sum:', sum(1, 2, 3));
// console.log('curried:', curried(1)(2)(3));

const add = (a, b) => a + b;
const diff = x => x - 1;

const composed = (a, b) => compose(a, b);
const curriedCompose = curry(composed);

console.log('composed:', composed(x => add(5, x), diff)(101));
console.log('curried compose:', curriedCompose(x => add(5, x))(diff)(101));

const curriedVarLength = curryVarLength(compose);
console.log(
  'curriedNew:',
  curriedVarLength(x => x * 1)(x => x + 1)(x => x - 5)()(100)
);
