# Practice Questions

1. Write a maybe function that, given a predicate (a function at returns a boolean value) and any other function, only calls the latter if the former returns true: maybe(x => x > 100, myFn). If the predicate returns true, the value of x should be passed to myFn. If it the predicate returns false, x should be returned unchanged.

2. Write a compose function that returns the result of all the functions passed into it, passing the returned value of a function to the function on its left: const foo = compose(x => x\*2, x => x + 1, x => x - 5); foo(100). Here, result should be 192.

3. Bonus: create three versions of compose: one that uses an iterative approach, one that uses Array.prototype.reduce, and one than uses recursion.

4. Note: assume that each function given to compose only accepts one parameter.

5. Write a reverse function that switches the order of the input function’s parameters: reverse((a, b) => a / b) should return (b, a) => a / b;

6. Use your reverse function to create a version of compose that runs its input functions from left to right: const foo = leftCompose(x => x\*2, x => x + 1, x => x - 5); foo(100); Here, the result should be 196.

7. Write a curry function that returns a curried version of the input function: const foo = curry((a, b) => a + b); foo(5)(4). Here, the result should be 9.

8. Use curry and compose to chain together these functions: const add = (a, b) => a + b; const dec = x => x = x - 1. Assume that, in the case of add, you always want the first parameter to be 5. You need to deal with the fact that compose assumes each function only has one parameter. Add has two parameters.

9. Write a branch function that takes a predicate function and two other functions or values. If the predicate is true, the result of the first function (or the first value) will be returned; if the predicate is false, the result of the second function (or the second value) will be returned.

10. Write your own versions of map, filter, and reduce with signatures like this: map(mapFunction, array); Using the Array.prototype functions internally is cheating.

