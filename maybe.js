const maybe = (predicate, myFn) => {
  const x = Math.floor(Math.random() * 200);

  console.log('Random Value of x:', x);

  return predicate(x) ? myFn(x) : x;
};
