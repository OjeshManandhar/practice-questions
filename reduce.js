const reduce = (reduceFunc, initValue, arr) => {
  let acc = initValue || 0;

  for (let i = 0; i < arr.length; i++) {
    acc = reduceFunc(acc, arr[i], i, arr);
  }

  return acc;
};
