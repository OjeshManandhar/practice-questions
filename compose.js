// Iterative
const compose_iterative =
  (...args) =>
  x => {
    let temp = x;

    for (let i = args.length - 1; i >= 0; i--) {
      temp = args[i](temp);
    }

    return temp;
  };

// Reduce
const compose_reduce =
  (...args) =>
  x =>
    args.reverse().reduce((acc, curVal) => curVal(acc), x);

// Recursive
const compose_recursive = (...args) => {
  const recursive = (x, index = args.length - 1) => {
    if (index < 0) return x;

    return recursive(args[index](x), index - 1);
  };

  return recursive;
};

module.exports = compose_iterative;
