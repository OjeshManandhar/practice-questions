const map = (mapFunction, arr) => {
  const newArr = [];

  for (let i = 0; i < arr.length; i++) {
    newArr.push(mapFunction(arr[i], i, arr, this));
  }

  return newArr;
};
